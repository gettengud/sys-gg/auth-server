// Establish the Bones of the App
const express    = require('express')
const bodyParser = require('body-parser')
const config     = require('cf-gg')
const app        = express()
const db         = require ('./src/db')
const surrigate  = require('./src/module/surrogate-services')

const async = require('async')

const LOG = require('logg')



//Server Startup Process
async.series([
    //Load Configuration
    function(next) { config.ScanConfigs(__dirname + "\\config\\"), next() },
    //Report Config TODO: Delete
    function(next) {LOG.LogReporting(config.global()), next()},
    //Connect to DB
    function(next) {
         db.Init(config.global().db, (result) => {
             if(result == "OK")
                next()
            else
                LOG.LogError(result)
            
         })
        
    },
    //Start the application
    function(next){
        LOG.LogReporting("Application Starting....")
        StartApplication();
    }
])



function StartApplication(){
    // X-Clacks-Overhead is critical.
    const xpressClacks = require('xpress-clacks-overhead')
    app.use( xpressClacks )

    // Allows us to read POST data
    app.use(bodyParser.urlencoded({ extended: false }))
    app.use(bodyParser.json())
    
    const Auth = require('./src/module/core_authentication')

    app.post('/surr-login', (req, res) =>{
        surrigate.Login(req, response => {
            res.json(response)
        } )
    })

    // Posting data to /login is possible from both a client
    // and a provider
    app.post('/login', (req, res) => {

        if(req.headers['gga-pub'] != config['auth_header'])
        {
            res.send("I'm sorry, I don't recognize you.")
            return;
        }

        //If a client key is provided we validated against
        //that by looking up registered client keys.
        if(req.body.clientKey)
        {
            
        }
        //If a client key is being sent by a provider
        //we just have to validate that the request is
        //coming from the provider's actual IP.
        else if(req.body.providerKey)
        {

        }



        req.body.clientKey
        req.body.providerKey
        req.body.username
        req.body.password   


    })

}