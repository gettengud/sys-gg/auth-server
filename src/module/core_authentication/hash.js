var crypto = require('crypto')
var salt = require('./salt')


var config = require('cf-gg')

var PKDF21 = {
    iterations:1000,
    keyLen:128,
    saltLen:64
}

var pepper = ```P7-NjYzQ2-LV?DWvZS*LMhdPXh-cUehAxHqE^u2aQgdd@L&=QH6$hbP_DWHZxPgrJdQtL-=H9*!65w+XUA=3!DD-y@?7nb$p3tp5mSFGmdVUsraGN+t$YW#q27FRFHFWL3p?fDV4MWw*-f#y%B8kYDS_m#8fz6Y#$uK8@6csr!^K^n-7ZdK$!qkt8xWa4NKT6KN@pq2z8zvzQUJTduumpv78gtgvvxMrrG8LNZ%rqPsez7$FmTwVadFC=hh@FR&jwG@$QC5TkDpq9u&@!^Cpx??5VfP*6NVqu2eDM=VaDRnkc6&gz-t?NJ=GSj^F5a-Y3a+A_qdy$5J6S?zC3%5=D4px?Pt3-un-&FD@w?qUr+fvAWZHerF%keFkZYwLvM^%2T9F3z9vW+*ZE8z9aVWJ7Nggb4LTS#G2VmrQy%HwataF6K8s4@SJ5Xx!RNhEX=KR!yaRLUe5WUJtg*jx2QG&PV66KDdg@Dj%5&!qbpDub9_qq5wQ@Xg8ptX=BWnsqY+UA=*RFu48GSTEP8tntn+=EV!vv6m7EF9j__a7Dgh?s*nZb8z%u43Xp*uYqF@kmqhnUPLB-W5mGk+Rjpnr@TT5Qd4EmF*69caf3-4AH+?@AhcT7mZ+-yuAU!6TQ%g?vKR$-YDjYmfKMP3EXHEwePn7=MFHMQpyJde!nJSJDMJ8=KQ_f^xrDm64J##LrU6mz2ERrSSMHLr3FKztPU3Wn$xweH4&RuLh2esVCJSF_#A#39Hbtr?wj%^W2_Xj@7=rsgH59-BmVTYEXzATM=dG+SW44tk@YLmGwc@K*P5rSU2+_kF-be8ytHEnDHZFX=_*rRQf6Ffa4?%*8@R?f+2teN!NAwDW8a+VRFEynRZ4y^AVa5DUwcx=HGdRsWbvSy&@-U7_7F_&4#WePA=zu?Yc55H#axU!qgvfUGsYChDAS^At2M7HXRrEv?yxB+uWZ5k*TceW67#bUU&@V9+CmDsfj*7E3!q=x95QMU5t6m$TXhfw%w6NHpf*jNA_MGMtYwLWfpt5vZgZjmc6&Apqj5-djUcAcDcu3Lj=Arkz4W9vF9pTS!b#J@W8w!HCf-8=kVE?k4LpXD_vpXsA38qWjJAT#-wZ#-z++GdRCHpv%%_JJ9tAgvRwrZ_=3N4J-F^_=8Sm2vr2mqpt4hx*ASs+%Qdwg!5s4K#WfvE9%Aszd5x+yx7XzRp$tC9N$f!Xp5Wt6QJZRLm_n2U=zrFUbMR4M%CJr4PUUMTL-pGwu8XrZv+7zMrpD%famHG#8+wV!4fsm$5$Qz8=S2Md^=^SS_D-sLM?er#+zgGZ2ruS8A9HE&qd4@taV$8M7$Bef%33MqZW4hs58uLfsaSjzVP4M-u=d=HZnV5&Anv&hs*@wJuS7=jspuYNfX7!HEZkXJQHgvMHC2T_d^#8gC5RncMShXT8tDnx9cLwPNse8v=YG-Hb6Sh$pc7LxuN*xJPvM6pHcTLd^=Q8aw^?Jfb-a6Zc6^&aNCBSZpF?Lj@4K^9s%=yhHXJc#y-tLvW_hRv5@WYU?GpWk@q4Q+!t?5@eqk9aLuLY@tNxy#hRUxk_d45Qr^CAc2vK!sCdbVLJRtV=fZJF9ghEt=cqMXrhN6jsw2ShCz@D9afDbp!tM^ZJ7n5yrE8CdMmhFp8br&AZHmGAzjU^*_4AVN_WncYT^ya@VA_LE!mayHTvjm*+=HS_rnzYJ+k*#$S9!ur!KvFfKxN7X4sGCLkZPR$c42QZXSq8mFTtx+kPUc6DCzHBxBkUBH8bGVN7NZbWMG=CCt-Z^hsdm=MUUPckV@7*xy7#efzmZMd4!g63TugJGJrLSE5r&TjA=eaJBHrD$Fx=CqJhrMwaxeueezERXfvZdcQY!cLcUVeseY7^Zw33yuxs2Hr?Wy?c$WjJgb38d+jEHxX$NCB-Sa&-_gDnJu3h!tQPq&tq%s&cqxURDA5wXJHZ5Q-5Gs-89jjZDkqfYPP5s*uQqH9mky+pGSrX2AxPa$R%M^z=#UcW!tsV93C@M?u%Wm$3*F2&UAyHUXB3$BF39e6WhqXG=PRCS$X%$qxEZj=N```

exports.hashPass = function(password, salt, callback) {

    password = Buffer.from(password).toString('hex')

    crypto.pbkdf2( password + pepper, salt, PKDF21.iterations, PKDF21.keyLen, 'sha256', (err,dk) => {
        if(err){
            callback(err)
        }
        callback(null, dk.toString('hex'))
    } )

}

exports.hashEmail = function(email) {
    let hash = crypto.createHash('sha256').update(email + pepper).digest('base64')
    return hash
}

exports.checkCredentials = function(email, password) {
    salt.Taste(hashEmail(email), (res) => {
        if(res != null)
            hashPass(password, res, (cb) => { 

                const submission = Buffer.from(res.saltine, 'hex')
                const comparison = Buffer.from(res.account.password, 'hex')

                let result = crypto.timingSafeEqual(submission, comparison)

            })
    })
}