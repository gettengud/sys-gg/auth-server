/** username.js
 *      Gives specific methods for handling usernames
 *      in GG Auth.
 */


exports.createFromEmail()

exports.createFromPhone()

exports.createFromIdentifier()





/** In the future I'd like to write an alternative set of one click logins
 *  that force you to prove ownership of the account in a way that doesn't
 *  create some creepy bridge between codebases.
 */

exports.createFromTwitter()

exports.createFromGitHub()