/** A provider key is an authentication secret shared between GG Auth
 *  and the provider of GG auth services for their application.
 *  
 *  These keys have expecatations which are agreed upon between GG Auth
 *  and the application in question.
 * 
 *  This module has methods to compare and create client keys.
 */

 const KeyModel = require('../../schema/providerKey').model;



/** Validate
 *      Check that the Provider's key is correct
 *      Check that the provider is coming from the expected IP
 */
 exports.validate = function( key, ip ){
    KeyModel.find(key)
 }