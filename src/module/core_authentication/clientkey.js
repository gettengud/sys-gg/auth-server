/** A client key is given to authenticate that an authentication set
 *  is coming from a recognized client. Client keys are nonces that are
 *  used only to log a client in and then pass along the account number
 *  to the appropriate provider.
 * 
 *  This module has methods to compare and create client keys.
 */



 /** Validate
  *     Check to see that a key exists.
  *     Check that they have the same IP.
  */
 exports.validate = function(key, IP){

 }

 /** Register
  *     Register for a client key,
  *     this can only be done by providing
  *     a valid provider key and the expected
  *     IP of the client.
  */
exports.register = function(){
  
}