mongoose = require('mongoose')

exports.Init = (configuration, callback) => {
    con = configuration.connection
    auth = configuration.auth
    mongoose.connect("mongodb://" + con.host + "/" + con.db, {useNewUrlParser: true, useUnifiedTopology: true })

    let db = mongoose.connection
    
    db.on('error', (error) => {
        callback(error)
    })
    
    db.once('open', () => {
        callback("OK")
    })

}