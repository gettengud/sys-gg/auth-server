var mongoose = require('mongoose');
var wikiMUDDb = require('../components/server/dbcons/wikimud-dbcon').wikiMUD;

var Privileges = require("./privileges").schema;

var accountSchema = new mongoose.Schema({
    email: {type:String, index:true, unique:true},
    password: String,
    priv: Privileges,

});

exports.model = wikiMUDDb.model('Account', accountSchema);
