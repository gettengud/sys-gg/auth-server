var db = require('mongoose').connection
var crypto = require('crypto');

var clientKeyToken = new mongoose.Schema({
    client: {type:String, index:true},
    key: String,
    created: String
});


exports.model = db.model('Client Key', clientKeyToken);



