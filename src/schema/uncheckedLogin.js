var db = require('mongoose').connection

var uncheckedLoginSchema = new mongoose.Schema({
    accountIdentifier: {type: require('mongoose').Schema.Types.ObjectId, index:true},
    ip: String,
    loginTime: Date,
    secret: string
});


exports.model = db.model('Unchecked Login', uncheckedLoginSchema);