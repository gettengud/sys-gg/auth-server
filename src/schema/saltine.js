var mongoose = require('mongoose');
var db = require('mongoose').connection;

exports.schema = new mongoose.Schema({
    account: {type:mongoose.Schema.Types.ObjectId, ref:"saltine"},
    saltine: {type:String, index: true}
});

exports.model = db.model('Saltine', this.schema);

