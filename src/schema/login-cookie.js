var db = require('mongoose').connection
var crypto = require('crypto');

var loginCookieSchema = new mongoose.Schema({
    token: {type:String, index:true},
    series: String,
    session: String
});

loginCookieSchema.methods.refresh = function ()
{
    this.token = crypto.randomBytes(16).toString("hex");
    return this.token;
}

exports.model = db.model('Login Cookie', loginCookieSchema);



