var db = require('mongoose').connection
var crypto = require('crypto');

var providerKeyToken = new mongoose.Schema({
    provider: {type: require('mongoose').Schema.Types.ObjectId, index:true},
    key: String,
    session: String
});


exports.model = db.model('Provider Key', providerKeyToken);



