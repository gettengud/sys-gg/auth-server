# GG Auth


## Surrogate Login Mode

The first supported method of authentication for GG Auth is called `Surrogate Login`. This allows you to log users in without processing any personal information at all!

## Surrogate Login Sequence

### Step One: Client Submits login to GG Auth
GG Server expects that client login requests contain appropriate client keys.
GG Server also expects that the login comes from an IP OTHER than the client's known IP's

### Step Two: Client receives response
GG Server figures out if the auth is correct

GG Server answers back with a ye or ney and also prepares an unchecked login for the client server to obtain.

### Step Four: Assuming Success, client submits login complete to the provider
This message needn't contain anything particularly important, once the provider receives this message-

### Step Five: The provider validates successful login with the authority server
The provider must submit the requestee's 